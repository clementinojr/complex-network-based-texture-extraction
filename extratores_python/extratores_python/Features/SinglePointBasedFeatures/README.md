## A. Single point based statistical plaque features
### A.1  Single/Multi ROI
1. GSM
2. Stratified GSM
3. JBA
4. PW
### A.2 Histogram based features
1. PPC1-10
2. Histogram
3. Multi-region histogram
4. Correlogram
