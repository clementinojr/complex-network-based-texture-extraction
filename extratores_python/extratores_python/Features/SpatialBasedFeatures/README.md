## B. Spatial based plaque features
### B.1 Early texture
1. FOS/SF
2. GLCM/SGLDM
3. GLDS
4. NGTDM
5. SFM
6. LTE
7. FDTA
8. Gray Level Size Zone Matrix (GLSZM)
9. FPS
10. Shape Parameters
### B.2 Later texture
1. Gray Level Size Zone Matrix (GLSZM)
2. Higher Order Spectra (HOS)
3. Local Binary Pattern (LPB)
### B.3 Morphological
1. Grayscale Morphological Analysis
2. Multilevel Binary Morphological Analysis
### B.4 Multi-scale
1. Fractal Dimension Texture Analysis (FDTA)
2. Amplitude Modulation – Frequency Modulation (AM-FM)
3. Discrete Wavelet Transform (DWT)
4. Stationary Wavelet Transform (SWT)
5. Wavelet Packets (WP)
6. Gabor Transform (GT)
### B.5 Other
1. Zernikes’ Moments
2. Hu’s Moments
3. Threshold Adjacency Matrix (TAS)
4. Histogram of Oriented Gradients (HOG)
